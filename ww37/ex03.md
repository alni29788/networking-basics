20A-ITT1-NETWORKING
2020/09/09

Exercise 3.3
Your Words

The 5 highlighted topics from my essay, was as following:
	NIC
	Internal View
	External View
	Host
	Network Emulation

Topic 1, NIC:
	Relevant Links:
		1) https://www.elprocus.com/network-interface-card-nic/
	NIC, Network Interface Card, an internal hardware component, allows the posibility for a computer network to communicate.		

Topic 2, Internal View:
	Relevant Links:
		1) https://moozer.gitlab.io/course-networking-basisc/Intro/Virtualization/
	The internal view is the term used when looking at the internal parts of the host or vitual machine. You can see stuff like what processors are running, and so forth.

Topic 3, External View:
	Relevant Links:
		1) https://moozer.gitlab.io/course-networking-basisc/Intro/Virtualization/
	The external view is the term used when describing what the external user can see from "the outside".

Topic 4, Host:
	Relevant Links:
		1) https://www.vmware.com/topics/glossary/content/virtual-machine
	The host machine is physical "host" machine. Every VM runs on its own OS and functions apart from each other, but they all run on the same host machine.

Topic 5, Network Emulation:
	Relevant Links:
		1) https://en.wikipedia.org/wiki/Network_emulation (decent sized article, locked)
	"Network emulation is a technique for testing the performance of real applications over a virtual network."

NOTE: 
It was difficult to find any sources regarding internal- and external view. Therefore, the link is to Morten's own video.